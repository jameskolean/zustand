// src/App.tsx
import { useState } from "react";
import create from "zustand";
import { devtools } from "zustand/middleware";
import "./App.css";

function App() {
  const [incrementAmount, setIncrementAmount] = useState("2");
  const [factLimit, setfactLimit] = useState(5);
  const mystore = useStore();
  const {
    count,
    increment,
    decrement,
    incrementByAmount,
    isFetchingFacts,
    facts,
    fetchFacts,
  } = useStore();
  return (
    <div className="App">
      <h1>Vite + React + Zustand</h1>
      <div className="card">
        <div className="button-bar">
          <button onClick={increment}>+</button>
          <button onClick={decrement}>-</button>
        </div>
        <p> count is {count}</p>
        <div className="button-bar">
          <input
            className="textbox"
            aria-label="Set increment amount"
            value={incrementAmount}
            onChange={(e) => setIncrementAmount(e.target.value)}
          />
          <button
            onClick={() => {
              incrementByAmount(Number(incrementAmount) || 0);
            }}
          >
            Add Amount
          </button>
        </div>
        <div>
          <p>Cat Facts</p>
          <ul>
            {facts != null &&
              facts.map((fact, index) => (
                <li key={index}>{JSON.stringify(fact)}</li>
              ))}
          </ul>
        </div>
        <div className="button-bar">
          <input
            className="textbox"
            aria-label="Set fact limit"
            value={factLimit}
            onChange={(e) => setfactLimit(Number(e.target.value))}
          />
          <button
            onClick={() => {
              fetchFacts(factLimit || 0);
            }}
          >
            Fetch Facts
          </button>
        </div>
      </div>
    </div>
  );
}

export default App;

// define the store
interface AppStore {
  count: number;
  facts: string[];
  isFetchingFacts: boolean;
  increment: () => void;
  decrement: () => void;
  incrementByAmount: (amount: number) => void;
  fetchFacts: (limit: number) => void;
}

export const useStore = create<AppStore>()(
  devtools((set) => ({
    count: 0,
    facts: [],
    isFetchingFacts: false,
    increment: () => {
      set((state) => ({ count: state.count + 1 }));
    },
    decrement: () => {
      set((state) => ({ count: state.count - 1 }));
    },
    incrementByAmount: (amount: number) => {
      set((state) => ({ count: state.count + amount }));
    },
    fetchFacts: async (limit: number) => {
      set({
        isFetchingFacts: true,
      });
      const response = await fetch(
        `https://catfact.ninja/facts?limit=${limit}`
      );
      const result = await response.json();
      set({
        isFetchingFacts: false,
        facts: (result.data as { fact: string }[]).map(({ fact }) => fact),
      });
    },
  }))
);
